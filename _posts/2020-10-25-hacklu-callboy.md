---
layout: post
title: "Hack.lu 2020 CTF write up: Callboy"
date: 2020-10-25
---

# Description

Have you ever called a Callboy? No!? Then you should definitely try it. To make it a pleasant experience for you, we have recorded a call with our Callboy to help you get started, so that there is no embarrassing silence between you.

PS: do not forget the to wrap flag{} around the secret

# Write up

This was quite a fun challenge, I learned a new way to use [Wireshark](https://www.wireshark.org/) and I can't stop to be amazed by this tool!

We get a pcap file and after looking a bit through it we can identify quite
quickly the communicating IP addresses. Coupling that with the description of
the challenge, we know that we have to capture some kind of communication
between two clients.

There is a lot of intented noise in the traffic: DNS requests to a lot of the
top alexas websites, but we don't care so much about it and it's an obvious
rabbit hole.

We know this is a recored communication, so probably telephony or video. After
some quick research, we realise it's possible to extract audio from pcap files!

We tried to fix a bash script for like an hour before realising that Wireshark does it for you [thanks to this link](https://phonesystemhelp.info/how-to-listen-and-extract-audio-from-a-wireshark-trace/).

After that, the challenge is a piece of cake, we need to follow the steps
explained in the web page on how to extract the audio, play it and we get the
flag!

![](img/callboy_1.png)

In itself it's quite an easy challenge when you know where to look. Some
interesting conclusions on that would be:

* situational awareness: how to get information from the noise of the traffic
* identify quickly protocols between relevant IPs
* get to know Wireshark much better!
