---
layout: post
title: "I gave “cyber” classes to kids and this is what I learnt"
date: 2018-04-24
---

Few months ago, I had the opportunity to join [Techies Labs](http://techieslab.org/), a small Belgian NGO giving coding classes to kids from 9-ish to 18-ish. I’ve been giving cryptoparties to many type of crowds in the past years and worked in a NGO defending civil rights online. Kids were a crowd I haven’t been in touch with these subjects yet although we thought about them when creating the hackerspace [Le Reset](https://lereset.org) back in the early days<sup>1</sup>.

# Children are the future

Sounds cliché, right? Although this is true. I’ve already been discussing “cyber” subjects and kids with some friends<sup>2</sup> and our conclusion was that usually activists leave children apart “because it is complicated adult stuff” or just because they forgot about them. Techies Labs was a great opportunity to tackle this.

I was really willing to speak about security and “all those hacker things” to kids because those subjects are real passion for me. I wanted to share that fascination without going into scary or too technical themes.

# Let’s play a game

(Un)Surprisingly, kids learn fast, I mean, really fast. As soon as you give them the keys to understand, it takes them seconds to draw conclusions and build their knowledge from there. Their minds work differently from adults and this was the biggest challenge for me: adapting myself to their quick learning curve.

To keep up to such a fast learning curves, I built all my classes on games (where they do all the work):

* Cryptography classes are a treasure hunt that requires code breaking capacities (with pen and paper of course),
* Internet infrastructure classes are about playing with pings, traceroutes, whois and other simple networking command line tools (after looking a cool undersea cable maps),
* Web classes are about playing with the developer console, understanding what happens there (on all of the tabs! I’m not limiting to just the HTML),
* Once, we did an information gathering class that was mostly about search engines and operators that allowed to do deeper research on their school website (e.g.: find all the PDFs on a website).

All of these classes were a success so far! Of course, it is important to stay around to help them in case they are stuck, but at no time giving them the answers.

The goals of each class were:
* Learn the material: usually the tools used, why there are used and what are they used for
* Learn some culture around the tools: for example on the search engine, we talked about Google, Duckduckgo why there are different etc.
* Work as a group: all the games require team work and a little competition between the team (for the thrill, everyone gets candy at the end anyway)

# Why it matters

We also discuss about recent events such as the Facebook/Cambridge Analytica scandal, how tracking works, how the Internet advertisement industry works… Without pushing too much politics in it of course, they have very interesting viewpoints on that. Fun fact, when I asked them who had a Facebook account, none raised their hand and someone said: “Ew, Facebook, my parents are on it. I’m not going there.” Well, it’s something.

![it's something](img/its-something.jpg)

But there are parts where children and adults aren’t different, it is the culture of the Internet. For example, how does the Internet looks like, what happens when you look something up in Google, why do you encrypt, how do trackers works… All this knowledge is what us, activists/hackers, can bring to the table. Explaining and demystifying technology to make it understandable is, according to me, our prime goal. With that we empower people and free them from the magical aspects of technology. In cryptoparties, we try to help people understand technology and security to allow them to control their communications. Teaching cyber to kids is opening a new world of possibilities and empower them to control their machine rather than the opposite.

Usually, after they discover something or pass a challenge, their eyes widen and they say in awe “I didn’t know, it was possible to do that.” and they go to their teammates to explain how to reproduce it. This is it. This is empowerment, this is what we are aiming at.


1. It was [Zora](https://twitter.com/zorakillerqueen)'s idea to teach kids.
2. What's up, Ivan?
