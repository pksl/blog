---
layout: post
title: (n)Vim shortcuts for the lazy
date: 2020-02-11
---

Shortcuts are great, you get faster through that square of grass and on computers you can do stuff with less movement and less distraction. It is almost like when you think something, you get it at the same time.

The power of shortcuts lies within the mnemonic memory of their name and the muscular memory of your hands.

All programs have shortcuts, Firefox (CTRL+L to edit the address, CTRL+D to bookmark...), on my desktop I set up ALT+T to spin a terminal, I use [Tilix](https://gnunn1.github.io/tilix-web/) and split my terminal with CTRL+ALT+D/R. Long story short, over the years, I have boiled down most of my routines into a couple of key strokes and do not think about moving the mouse anymore.

## Vim your fingers

Vim has literally a thousands of shortcuts, but they are not really shortcuts, they are functions calls and they make sense. You can refer to this historical [answer](https://stackoverflow.com/questions/1218390/what-is-your-most-productive-shortcut-with-vim) on Stackoverflow or learn most of them with [cheatsheets](https://gist.github.com/awidegreen/3854277).

But I have compiled during the years, a couple of shortcuts for a couple of keystrokes that I realized I was repeating over and over again.

* **your hands do not have to move**:

```
inoremap jj <ESC>:w<CR>
inoremap kk <ESC>:w<CR>
```

Probably the most Vim shortcut, just double hitting "j" or "k" in insert mode will save your file but not return to insert mode. I have a return to save+return to insert mapped (see next section).

* **follow the leader**: the <Leader> key will allow you to do a bunch of functions call with this one modifier, think about it as the WIN/CMD key on your keyboards.

I map my <Leader> to "," and use for anything to do with buffers and meta functions.

```
let mapleader=","
map <Leader>, :w<CR> " double "," will save the file
inoremap <Leader>, <ESC>:w<CR>i " in insert mode, double "," will save the file and return to insert mode
map <Leader>[ :bprevious<CR> " go to previous buffer
map <Leader>] :bnext<CR> " go to next buffer
map <Leader>l :buffers list<CR> " list all buffers

" FZF and Ripgrep
inoremap <Leader>F <ESC>:FZF<CR>
nnoremap <Leader>F :FZF<CR>
inoremap <Leader>G <ESC>:Rg<CR>
nnoremap <Leader>G :Rg<CR>

" NERDTree
map <Leader>k :NERDTreeToggle <CR> " open/close NERDTree

```

* **quick close all opened non-buffers**: If you a have a lot of plugins that open a lot of windows, you can close most of them with this call:

```
map <Leader>a :cclose <bar> lclose <bar> pclose<CR>

```

* **center searched terms**: These mappings will make it so that going to the next one in a search will center on the line it's found in. I found those roaming around the Internet.

```
nnoremap n nzzzv
nnoremap N Nzzzv

```

## Closing thoughts

There are plugins that wrap all those shortcuts and even more, but I feel they are sometimes hard to learn and do not match the patterns I have in my hands. They point is not to remap, reinvent and rewrite Vim (well you can), but to learn its in-built functions. This why I try to keep those shortcuts to a minimum and maybe wrap series of keystrokes I do often to shortcuts.
