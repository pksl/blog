---
layout: post
title: I pulled a prank on Google venue
date: 2018-12-22
---

Few months ago, I pulled a little prank on Google's very special little venues that are installed everywhere in Europe. The famous "ateliers" that receive a very wide range of [mixed responses](https://wiki.fuckoffgoogle.de/index.php?title=Main_Page).

Well, this little prank also had a very wide range of mixed responses. I don't really care now, it has been few months.

# Once upon a time

In the open space where I worked, there was a huge space that was rented by Google to install one of its ateliers where they're going to teach people how to use AdWords for their businesses and call themselves hackers. A that time, there was already a pretty strong mobilisation against Google in Berlin (#fuckOffGoogle) and some stuffs where happening in France as well.

After their installation, I started sharing a bunch of anti-Google stickers with other friends and people working around and we started sticking them here and there. Most of them were quickly removed but not by the cleaning crew. Some stickers stayed because the cleaning crew doesn't clean the walls. So far, nothing big happened. The manager of the atelier wasn't very happy.

One day, it was raining, we went with friends and colleagues eat our lunch in the Google atelier because we didn't wanted to spend our break over our keyboards.

We weren't really supposed to be there, but no one was having their little mass to online marketing we were allowed to stay (how gracious). It's important to say that the atelier was most of the time empty but wasn't considered a chill space, so it wasn't allowed to chill there which sucks, cause the installations were quite nice.

Indeed, there were green plants, wooden boards, dim lights and brand new chromebooks.

Brand new chromebooks on display with "Test your security and privacy here" written next to them.

# Hack the planet

With my little knowledge of chromebooks, I tried to ticker things around but as they weren't on administrator mode, it was limited. I just opened a terminal, wrote: "ahaha you've been hacked" and left. I set the second chromebook to display a `top` and got back to eat my overpriced lunch.

No long passed before a member of Google' staff passed and looked worried at the black screens displaying green letters. She ran off to her boss and came back grabbing quickly the computers under her arms to look at them behind the counter.

Ah yes, the terminals were put as full screen of course.

Few minutes later, the manager asks around "is there a developer around here, we might need help". Good for them a coding school were having their end of the class party and there were a good dozen of devs. A friend of mine went.

I was looking at his face while eating and expecting him to understand the prank. But he didn't and looked confused. I still don't know if he was confused by the the stress and the anger of Google' staff or because of the prank.

My heart broke and I went to help them: "Hi guys, I'm a security expert, maybe I can help?". I hit CTRL-C and cleaned the screens.

— That wasn't much. Here, your computers are clean. I said.

— Who did that? Said the Google manager, almost shouting. Her face was swelling red of anger.

— I didn't do it. Said my friend. I am not part of his startup - he pointed at me -, I'm with another startup and our students don't have the skills to do that.

— Your computers are clean, don't worry. I added.

— It's a total lack of respect of us and Google! Shouted the manager, directly looking at me, I guess she understood. It is not funny!

— It's a little funny, I joked shrugging.

Maybe that was a mistake.

— I'm going to tell my boss! With which company are you?

— X, said my friend which I guess wasn't such a friend at that moment.

— Ok, look, I did it. But it's just a little prank. No need to get crazy about it. Your network is safe, the integrity of your computers as well. Nothing to worry about. I said trying to calm everyone down.

— I don't care, I'm calling my boss! We open a space for you here and you just don't respect it! Google is never going to stay here if you keep fucking with us.

— Well, call your boss. Call Larry Page, I don't care. I said and left.

![](img/fuckoffgoogle1.png)

# Then shit hit the fan

I came back to my office and went to see my boss: "I just made a prank to Google, just giving you heads up if you hear about that". I think her answer was something like "lol" and I got back to work.

Few minutes later, the big boss of the openspace, the Mr. CEO came to our office and after few seconds, my boss came to the classroom and asked me to see her in her office.

— So, I heard you pulled a prank on Google? Asked Mr. CEO.

— Mh, yeah. I said.

— They are not very happy. What did you do?

— I just opened a terminal, wrote: "ahah you've been hacked" and left. Didn't touch their computers, network or whatever.

— Can you promise me that?

— Yes.

— Ok, we are going to talk to them. They are really pissed.

I got back to work, saw the COO and the CMO passed in front of my office waving and giving me air-high-five and looking very happy. I guess they aren't found of Google either. My boss and Mr. CEO went to talk to Google' staff and I kept taking care of my stuff.

Later, I got chance to have some information from my boss. She told me that Google was really angry. They threatened to leave the openspace and even contacted Google Belgium because they thought I tweeted the screen. Which would have cause them a bad marketing hit. The marketing contract, which is evaluated at millions, between the openspace and Google was at stake.

All for a really really stupid prank. They aren't much hackers now.

They even threatened to block their space for our students for their presentation. Which, between us, is a bit vicious because they have nothing to do with that and even haven't heard about it.

To conclude, my boss was forced to publish on our slack this message: "Dear all, it's forbidden to hack, prentend to hack or joke about hacking Google.". I was suspended for 3 months and Mr. CEO never talked to me again. No regrets.

![](img/fuckoffgoogle2.jpg)
