---
layout: post
title: Nothing says OpSecFail better than missing garbage
date: 2018-12-10
---

When you start learning about security and going to cryptoparties, first steps are encrypting your device, your phone. Then, you convince your friends to use secure messengers and maybe, if you're really convincing, email encryption. Regularly, you check if your Internet connection is secured and maybe start to become a little paranoid about your habits. Maybe, you switch to Linux and try to control all your digital behavior as much as you can.

You become increasingly aware and you start hardening your installation, you know your computer, your passwords are secured and your communications are encrypted. You know how to use Tor or pay for a VPN. You have long discussions about how to be more secure online and offline. You learn about Threat Modeling, APT, Cyber Crime, Russian hackers, Chinese troll farms... You speak about the NSA, the GCHQ, the DGSI, the GRU at parties, at conferences. You have one word on your lips: security.

Everyday, you feel more confident about the space you control. You try to help other people because you know it's important.

Then, one night, someone steals your paper garbage.

![](img/brokenlock.jpg)


# It isn't a bug, it's a failure

>"I think the paper garbage is missing." — Someone, somewhere

You know it's impossible because it's ridiculous. However, somewhere in you head, you remember to have taken them out, and you also saw all the garbage still in the street, untouched. But not in front of your house. Your porch is clean.

You try to remember what you threw. Important papers, identifying information about you, your family, your workplace that you tore, but you know with some patience it's recoverable. But it's too late to think about that data, it's lost.

You lost control over it.

It's infuriating. You put so much effort to secure your computer and your friends and it was just a stupid paper that broke all your defenses. You think about the companies you mocked for losing client data. Well, who's laughing now?

# Threat Modeling, do you speak it?

In my lastest cryptoparties, I tend to underline the importance of physical security. Based on the work of Tactical Tech on [*Holistic Security*](https://holistic-security.tacticaltech.org/) or the [*Smart Girl's Guide to Privacy*](https://www.goodreads.com/book/show/25644910-the-smart-girl-s-guide-to-privacy), I believe it's important to secure people and data and when you're talking to individuals worrying about an ex, a coworker, a stalker before the police or the fucking NSA, it makes more sense for you as a trainer to adapt yourself to your audience.

I try to speak more about secured behaviors than to "sell" the best tools. By involving the audiences in building a scenario, they understand much better the importance of certain tools and often, they find new attack vectors or new defense techniques.

In the case I'm describing above, none of any of the things I learned  about or taught in a crypotparty works. No GPG, no OMEMO, no add-on for your browser, nothing's there. Maybe some day, someone threw a shredded paper in different garbage can but at your place, how many paper garbage cans do you have?

The technical solution here would be to buy a shredder, those machines that can shred your papers and/or hard drives into unrecoverable pieces. However, those machines are costly and that might indicate to your stalker they have been spotted (it's something you might want to indicate or not, depending on your situation). Also, it doesn't solve your main problem: someone who is motivated enough to come at night to your place to steal your garbage.

There isn't much that you can do except catching that person red-handed and sending him to the police. This won't fix your feeling of having been violated or erasing the data you lost control over, it might not even fix the problem itself.

# Silver bullets only kill werewolves

There are no "silver bullet" solutions in security. Failure is something happpening regularly to every one, to every organisation. Being security aware is having planned that kind of scenarios and being able to recover your defenses based on the experience of previous situations (wins or fails).

It's impossible to have untouched defenses and security isn't a zero sum game, you have to be ready to lose some feathers in the battle because security only buys you time.

As from the moment your data is written on a disk, on a paper, it's just a matter of time before someone reads it. Maybe you'd be lucky enough to make the time between it's written and read so long as we can consider it will never be written. Encryption can do that, not writting data as well.

It's important to remember that security buys you time, time allows you to build an operation and carrying it out, time lets you live, time is what matters in life. Security is a game against Time.

In conclusion, this example is an illustration that maybe in our trainings, cryptoparties and other security talk we often overlook the physical aspect of security (offensive as defensive). Maybe in the future, we should give more importance to this side of security. What do you think?

![](img/overcome.jpg)
