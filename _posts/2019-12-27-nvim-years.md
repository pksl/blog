---
layout: post
title: "I spent 3 years configuring (n)Vim and this what I learnt"
date: 2019-12-27
---

# TL;DR: just get me the plugins

(I use neovim so some plugins might not entirely work for vim)

You can check my full configuration [here](https://gist.github.com/christalib/97bbe55e6ceb708d268eb27cd53fd266). I did not mention mappings because I think this is very personal and changes from one person to another. I only take care of plugins in this article.

## the essentials

If you use [`vim-plug`](https://github.com/junegunn/vim-plug) as a plugin manager, just put those in your `.vimrc` or your `init.vim` if you are using neovim.

* Plug 'dense-analysis/ale' : Ale is a very powerful linter and fixer that works with many programming languages, a real must have (especially because it work almost out of the box) [link](https://github.com/dense-analysis/ale)!
* Plug 'Shougo/deoplete.vim' : deoplete is a completion tool that work especially well with neovim but you can find other completion tools such as CoC.  You can also add deoplete with specific languages, here with jedi for Python, deoplete-go for Go and solargraph for Ruby:
  ```
    if has('nvim')
      Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
    else
      Plug 'Shougo/deoplete.nvim'
      Plug 'roxma/nvim-yarp'
      Plug 'roxma/vim-hug-neovim-rpc'
    endif
    Plug 'deoplete-plugins/deoplete-go', { 'do': 'make'}
    " do not forget to install jedi
    Plug 'deoplete-plugins/deoplete-jedi'
    " do not forget to install solargraph
    Plug 'uplus/deoplete-solargraph'
    call plug#end()
  ```
[link](https://github.com/Shougo/deoplete.vim).
* Plug 'fatih/vim-go' : as a Go developer, `vim-go` is a must have that allow you to check the documentation and go to the definition easily. It has many other cool features, you should check it out [link](https://github.com/fatih/vim-go).
* Plug 'scrooloose/nerdcommenter' : commenting made easy [link](https://github.com/scrooloose/nerdcommenter).
* Plug 'scrooloose/nerdtree' : looking in the tree made easy and a bitter more understandable than the native one (at least for me) [link](https://github.com/scrooloose/nerdtree).
* Plug 'mattn/emmet-vim' : autogenerate tags when writing HTML, the most useful things for web-developers [link](https://github.com/mattn/emmet-vim).
* Plug 'junegunn/fzf' : fast search at the tip of your finger, never use nerdtree again [link](https://github.com/junegunn/fzf).

## making your coding more comfortable

* Plug 'jiangmia/auto-pairs' : auto-complete parenthesis and other brackets [link](https://github.com/jiangmia/auto-pairs).
* Plug 'tpope/vim-surround' : a bit hefty to learn to use but after that your will gain so much time and keystrokes when wrapping your functions and blocks [link](https://github.com/tpope/vim-surround).
* Plug 'itchyny/lightline.vim' : a nice tool line for you vim with many themes and very customizable [link](https://github.com/itchyny/lightline.vim).
* Plug 'maximbaz/lightline-ale' : display your linter errors and warnings in lightline [link](https://github.com/maximbaz/lightline-ale).

## git
* Plug 'airblade/vim-gutter' : display your changes in a gutter, undo them and preview them [link](https://github.com/airblade/vim-gitgutter).
* Plugs 'itchyny/vim-gitbranch' : display the git branch in lightline in your active git repo [link](https://github.com/itchyny/vim-gitbranch).
* Plug 'junegunn/gv.vim' : not necessary but a nice explorer for your git repo and commits, useless if you use tig [link](https://github.com/junegunn/gv.vim).

## bonus
* Plug 'junegunn/goyo.vim' : get vim even more distraction free [link](https://github.com/junegunn/goyo.vim).
* Plug 'psliwka/vim-smoothie' : smoother scrolls [link](https://github.com/psliwka/vim-smoothie).
* Plug 'mhinz/vim-startify' : a nice startup page with possibilities to get your previous buffers up and running at the tip of your fingers [link](https://github.com/mhinz/vim-startify).

## colors
* Plug 'ayu-theme/ayu-vim' : a orange based colorscheme amazing with redshift [link](https://github.com/ayu-theme/ayu-vim).
* Plug 'articicestudio/nord-vim' : a blue based colorscheme, perfect for day work [link](https://github.com/arcticicestudio/nord-vim).


# About it

Vim is an amazing tool, from the first day I heard about it, I had one idea in mind: using it. At that time, I didn't understood the purpose, I was not a developer not I was close to anything from programming. Years were passing by and I kept hearing about how awesome it was without having really tried it.

One day I became a developer, I tried using Vim but the overhead was too big, too complex. I couldn't keep my head around what I was supposed to do (code) and the editor. So I used Sublime Text and everything went as smoothly as it should go for someone learning to code.

Months were passing, Vim was calling me like a mermaid in the mist. I move to Vim mode in Sublime and used very little options to it. I tried to keep learning but it was increasingly difficult and none seemed to really make me a better developer. I was just faster at deleting lines. VSCode happened then, and I used the Vim mode as well but there trying to increasingly understand new concepts of development, as for Sublime, I tweaked VSCode as much as I could. I realised it isn't some much about the tool but the way you code: everyone needs different things.

I could only understand that with years of into development: what do I need fast, what type of feature I need at the tip of my fingers. This is the core reason you configure Vim: to be faster, to stop thinking and to match your patterns of working. Vim is the ultimate productivity machine but can also be a deep rabbit hole if you get sucked by it.

For example, I need to split screens fast (`C-W + v|s`), open terminals (:term in neovim), go back to function definitions (:GoDef in vim-go) and not repeat the same code over and over (dot operator and macros). Clearly, VSCode can do that, but for some reasons it didn't work for and we parted ways after 6 months.

I finally gave Vim a couple of unfocused weeks, with stackoverflow and cheatsheets all over my desk, I was learning it day by day, until I matched all my mind-paths to something that existed in Vim. I was getting there and it felt awesome.

This is when I realised something.

It seems that looking for a useful and productive code editor or IDE is first of all a look into your own development procedures and techniques. It is not about being cool or being fast, it is about being smart. What matters here is to become one with your tool, understanding it but also bending it to what you need.

What do you need when you code? What are the features that make you faster? Do you need split windows? Are you distracted by the large amount of features in an IDE (I know I am)? Are linters useful for you? Do you need to autofix your syntax? Color or not? There are many questions, many plugins and so much time lost in the depth of configuration.

Essentially, it is something you can only know if you have been developing for some time, but aren't too much linked into a tool. Vim takes time but I guess like the Stockholm syndrome, when you get into it, it's hard to discard all the work you put into it.

Finally, after all those days spent looking into the void of my own reflection in the screen. The almost perfect configuration appeared to me like the sun on a winter morning. It was it. I knew it.

Code was easy, things were happening like I wanted, how fast and nice as I wanted.

I found the exit. I exited Vim. Now we are walking together the paths of development.
