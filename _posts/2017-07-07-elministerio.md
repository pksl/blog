---
layout: post
title: "Bypassing 401 and geoblocking"
date: 2017-07-07
---

# The issue

I am a big fan of the Spanish TV serie "[El Ministerio del Tiempo](http://www.rtve.es/television/ministerio-del-tiempo/capitulos-completos/)" (The Ministry of Time). It a show about time travelling and Spanish history. It is quite refreshing to have scifi from something else than a US-centered point of view.

Until the 3rd season, the show was avalaible on the website of the Spanish public media [webpage](http://www.rtve.es/) and was accessible anywhere easily.

However, since then the show became famous, so much there is a US rip off called ''Timeless''. So the budget of the series increased and Netflix got also involved. For some reason, it was enough for the producers to geoblock the videos and put a time limit on their online availability (which is ironic for a time travelling serie).

Long story short, I wasn't able to see the new episodes and was very frustrated.

![EMDT](img/emdt.jpg)

# Try again

I didn't wanted to find a torrent, because it was too easy. The website is plain HTTP except if you login and most of the content is freely available. Also, as content funded by public money should be available to anyone, I wanted to find a smart long-lasting solution.

As the error message displayed told me that my content wasn't available in my country, I needed to reroute my connection to Spain.

Before that, I tried to access the website with `curl`, `wget` or [`youtube-dl`](https://rg3.github.io/youtube-dl/) but got the same [401 HTTP error](http://www.checkupdown.com/status/E401.html). No luck (and I am pretty bad at forging HTTP headers).

After trying some proxies and not having a VPN going out in Spain and after some advices from friends, I decided to use
[Tor](https://torproject.org) and reroute the exit nodes to Spain and torify `youtube-dl`. It was easier than I thought.

What does that mean? It means that I tell youtube-dl to be rerouted through Tor that is going out in Spain and to download my video. It is slower because it uses the Tor network but it works.

# Howto

On Linux (I use Debian), after downloading Tor through the sources, you need to modify the `/etc/tor/torrc` and choose your exit nodes, in my case: `exitnodes {es}`.

Then, you run: `torify youtube-dl [link to the video]` in a terminal and tada!

# El tiempo es lo que es

It *is not a safe use of Tor* and people *[do not recommend it](https://tor.stackexchange.com/questions/64/how-can-bittorrent traffic-be-anonymized-with-tor/68#68)*  especially if you are going to torrent files. In that case,  it is better to use a dedicated machine (VPS or real), with a VPN in a safe company and/or country.

But as I said before, public funded content should be accessible to anyone, anywhere. Geoblocking is a fraud and  an idiotic invention to forbid sharing content. I do not agree with that.

If you are interested and, here are torrent files for the episodes of the 3rd season that are out so far (it is going to be updated as soon as they are put online).

I don't seed all the time, please be patient.

* [S03E01](https://framadrop.org/r/dxXbGut3SG#6/EYP7+JrUW3sZdMLLqoR40Uc8H9tOEg37+hGkFsdok=) (Con el tiempo en los talones - episode 22)
* [S03E02](https://framadrop.org/r/-rsh4oijPH#FYKXiHu7wRT/Oi5rp5jdSrHTkuKGJ1033iZRHxUUAk8=) (Tiempo de espias - episode 23)
* [S03E03](https://framadrop.org/r/s9ltyFBKo7#E37YNpuTFEyBqzEwCOblQQUKlrL7UX8xoFwoeA7K448=) (Tiempo de hechizos - episode 24)
* [S03E04](https://framadrop.org/r/YZJMD6ZU6G#LIDqws9p7lBbozW+nDI5mnK+stArT7e5C5CJnrlkixU=) (Tiempo de illustrados - episode 25)
* [S03E05](https://framadrop.org/r/H1A5f1yl9B#899Hz/0gUXpBAmqcMNuvNo/uRvg/UWw7iigrmW5f2s0=) (Tiempo de esplendor - episode 26)
* [S03E06](https://framadrop.org/r/nTx65JtmuL#fFTge7cE+RLUSgqeAB3qV4L0yBOPVFFXYL4H1NG/Q1U=) (Tiempo de esclavos - episode 27)
* [S03E07](https://framadrop.org/r/j2DGOO2Uoc#KdPJXd21vwYiPbyhL/wYu+T+m4cEwsHIQsyTPgNRjjE=) (Tiempo de censura - episode 28)
* [S03E08](https://framadrop.org/r/zUOXitY2UK#3HBW4rCUENvQSWYodblDj0vinYA/0gD6effYf6AvPRs=) (Tiempo de conquista - episode 29)

![yipikaihei](img/emdt2.gif)
