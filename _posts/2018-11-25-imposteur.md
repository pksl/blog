---
layout: post
title: It is not because you are (an imposteur)
date: 2018-11-25
---

It has been a crazy year. I already wrote about it in a previous [blog post](https://pksl.frama.io/blog/bazaar/2018/06/15/oneyear.html). I've experienced ***a lot*** of new challenges and to be honest it hasn't always been easy. Nonetheless, those challenges lead me, they give me the kick. I'm not interested if something is easy.

After few months of struggling between the golden life/work balance, I managed
to open some days for ***actual*** holidays and it was amazing. I took this time to just talk with people, wander around, make shitty pictures, enjoy the company of very good friends and reconnect with others.

During that time, I also took the time to reflect on my so-called carreer. After a year of not-bad freelancing, I need to think about my next steps. I've taken some choices for which I am not entirely satisfied, so it was a perfect time to reflect upon that.

And what is the best way to think about the future but looking back, not just the past year but little further back? So I did.

Funny thing I realised is that when I started project like the radio show [Source](http://src.brussels) or decided to make a newspaper with friends, I realised I had absolutly no clue about what I was doing nor where I was going. There was a certain type of energy at this time that you only have by being young, clearly stupid and obnoxiously inconsequent.

![Smells like teen spirit](img/teenspirit.jpg)

But nevertheless, I did it. We did it and somehow we managed it. Of course, it wasn't always easy, always great but it was worth it. And this is what I took from this time: this obnoxious raw energy.

# Mirror, mirror

I don't think people know what they are doing on a daily basis, I believe that we all make it up as we go. There is no way to know the future, we are just being driven by something, a feeling, a goal, and realise after few years where we are thanks or because of it.

With the success and failures, my choices became more and more precise and I guess that's what you call experience. It became easier to make choices and ***stick to those choices***, which means taking bigger risks, bigger challenges, bigger choices.

Those risks often implies that you feel like you don't belong that you're not good enough, the infamous **imposteur syndrom** that is so prevalent in our communities. I don't think there is a cure for that, I have no idea how to fix mine either. But I've found a way to live with it, I'm not sure it'd work for everyone.

My imposteur syndrom is rooted in a fear of failure and loosing the trust of people who are trusting me. For the first part, it is impossible to be perfect but it is possible to work as hard as you can to be able to at least ***look as perfect as you can be***. No one is expecting a junior to be as good as someone who worked 15 years in the same field. ***Sometimes you fail, it doesn't matter how many times you failed but how many times you started again***. So I keep going and maybe when the dust settles, I see that everything went fine.

For the second part, well, I guess you don't have control on that. Just be your best self and somehow you'll manage it. People understand you aren't perfect, they aren't either. The uncompromising judge is only yourself so be easy on yourself.

# Bigger than punk rock

We are surrounded by amazing people, people who have done great things and who are still rocking your socks off at each of their new projects. It can be intimidating but everyone has a story to tell and everyone is capable of many things. Everyone, even the people you admire, feels a little like an imposteur at times and it's okay. We are mere humans.

I remember someone once told me: "The best time to start something was 10 years ago or now". I think a lot about that sentence, of course if you started something 10 years ago, you'd be really good. But no one is stopping you to start it today either.

***Be the best version of yourself, be the person you want to see in the mirror. Be excellent to yourself and to the others. Rise and shine. You matter and you can do it.***

![Do epic shit](img/epic.png)
