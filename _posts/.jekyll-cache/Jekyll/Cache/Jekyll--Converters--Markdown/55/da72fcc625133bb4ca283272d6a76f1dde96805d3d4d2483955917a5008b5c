I"�2<p>Working on some projects, I needed to write continuous data to JSON files in Go. So as I found various tutorial but none really gave me what I wanted, here is mine!</p>

<h1 id="get-the-files-ready">Get the files ready</h1>

<p>The last thing you want is panics on missing files, so you need to check if the file exists first (here is the <a href="https://golangcode.com/check-if-a-file-exists/">example</a> I used). I tweaked it a bit more so I could create the file if it didn’t exist.</p>

<div class="language-go highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">func</span> <span class="n">checkFile</span><span class="p">(</span><span class="n">filename</span> <span class="kt">string</span><span class="p">)</span> <span class="kt">error</span> <span class="p">{</span>
    <span class="n">_</span><span class="p">,</span> <span class="n">err</span> <span class="o">:=</span> <span class="n">os</span><span class="o">.</span><span class="n">Stat</span><span class="p">(</span><span class="n">filename</span><span class="p">)</span>
        <span class="k">if</span> <span class="n">os</span><span class="o">.</span><span class="n">IsNotExist</span><span class="p">(</span><span class="n">err</span><span class="p">)</span> <span class="p">{</span>
            <span class="n">_</span><span class="p">,</span> <span class="n">err</span> <span class="o">:=</span> <span class="n">os</span><span class="o">.</span><span class="n">Create</span><span class="p">(</span><span class="n">filename</span><span class="p">)</span>
                <span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
                    <span class="k">return</span> <span class="n">err</span>
                <span class="p">}</span>
        <span class="p">}</span>
        <span class="k">return</span> <span class="no">nil</span>
<span class="p">}</span>

</code></pre></div></div>

<p>I know we could return the file itself, but I didn’t really needed it in this case.</p>

<h1 id="save-the-marshall-ryan">Save the Marshall Ryan</h1>

<p>I am not entirely sure I understand marshalling completely, I have to confess. What I know is that the Go handle is ready the bytes in an array and you need to send it to a receiving structure to be able to have it human-readable.</p>

<p>And of course the same way back to write to it.</p>

<div class="language-go highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">type</span> <span class="n">MyStruct</span> <span class="k">struct</span> <span class="p">{</span>
    <span class="n">StructData</span> <span class="kt">string</span> <span class="s">`json:"StructData"`</span>
<span class="p">}</span>

<span class="k">func</span> <span class="n">main</span><span class="p">()</span> <span class="p">{</span>
    <span class="n">filename</span> <span class="o">:=</span> <span class="s">"myFile.json"</span>
    <span class="n">err</span> <span class="o">:=</span> <span class="n">checkFile</span><span class="p">(</span><span class="n">filename</span><span class="p">)</span>
    <span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
        <span class="n">logrus</span><span class="o">.</span><span class="n">Error</span><span class="p">(</span><span class="n">err</span><span class="p">)</span>
    <span class="p">}</span>

    <span class="n">file</span><span class="p">,</span> <span class="n">err</span> <span class="o">:=</span> <span class="n">ioutil</span><span class="o">.</span><span class="n">ReadFile</span><span class="p">(</span><span class="n">filename</span><span class="p">)</span>
    <span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
        <span class="n">logrus</span><span class="o">.</span><span class="n">Error</span><span class="p">(</span><span class="n">err</span><span class="p">)</span>
    <span class="p">}</span>

    <span class="n">data</span> <span class="o">:=</span> <span class="p">[]</span><span class="n">MyStruct</span><span class="p">{}</span>

    <span class="c">// Here the magic happens!</span>
    <span class="n">json</span><span class="o">.</span><span class="n">Unmarshal</span><span class="p">(</span><span class="n">file</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">data</span><span class="p">)</span>
<span class="p">}</span>

</code></pre></div></div>

<p>Now we can work and build more structures to our JSON file.</p>

<div class="language-go highlighter-rouge"><div class="highlight"><pre class="highlight"><code>
<span class="c">// MyStruct is an example structure for this program.</span>
<span class="k">type</span> <span class="n">MyStruct</span> <span class="k">struct</span> <span class="p">{</span>
    <span class="n">StructData</span> <span class="kt">string</span> <span class="s">`json:"StructData"`</span>
<span class="p">}</span>

<span class="k">func</span> <span class="n">main</span><span class="p">()</span> <span class="p">{</span>

    <span class="p">[</span><span class="o">...</span><span class="p">]</span>
    <span class="n">newStruct</span> <span class="o">:=</span> <span class="o">&amp;</span><span class="n">MyStruct</span><span class="p">{</span>
        <span class="n">StructData</span><span class="o">:</span> <span class="s">"peanut"</span><span class="p">,</span>
    <span class="p">}</span>

    <span class="n">data</span> <span class="o">=</span> <span class="nb">append</span><span class="p">(</span><span class="n">data</span><span class="p">,</span> <span class="o">*</span><span class="n">newStruct</span><span class="p">)</span>

    <span class="c">// Preparing the data to be marshalled and written.</span>
    <span class="n">dataBytes</span><span class="p">,</span> <span class="n">err</span> <span class="o">:=</span> <span class="n">json</span><span class="o">.</span><span class="n">Marshall</span><span class="p">(</span><span class="n">data</span><span class="p">)</span>
    <span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
        <span class="n">logrus</span><span class="o">.</span><span class="n">Error</span><span class="p">(</span><span class="n">err</span><span class="p">)</span>
    <span class="p">}</span>

    <span class="n">err</span> <span class="o">=</span> <span class="n">ioutil</span><span class="o">.</span><span class="n">WriteFile</span><span class="p">(</span><span class="n">filename</span><span class="p">,</span> <span class="n">dataBytes</span><span class="p">,</span> <span class="m">0644</span><span class="p">)</span>
    <span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
        <span class="n">logrus</span><span class="o">.</span><span class="n">Error</span><span class="p">(</span><span class="n">err</span><span class="p">)</span>
    <span class="p">}</span>
<span class="p">}</span>

</code></pre></div></div>

<p>That does the work! What are your techniques to write to JSON file and append new data to it?</p>

<p>Here is the whole code:</p>

<div class="language-go highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">package</span> <span class="n">main</span>

<span class="k">import</span> <span class="p">(</span>
	<span class="s">"encoding/json"</span>
	<span class="s">"io/ioutil"</span>
	<span class="s">"os"</span>

	<span class="s">"github.com/sirupsen/logrus"</span>
<span class="p">)</span>

<span class="c">// MyStruct is an example structure for this program.</span>
<span class="k">type</span> <span class="n">MyStruct</span> <span class="k">struct</span> <span class="p">{</span>
	<span class="n">StructData</span> <span class="kt">string</span> <span class="s">`json:"StructData"`</span>
<span class="p">}</span>

<span class="k">func</span> <span class="n">main</span><span class="p">()</span> <span class="p">{</span>
	<span class="n">filename</span> <span class="o">:=</span> <span class="s">"myFile.json"</span>
	<span class="n">err</span> <span class="o">:=</span> <span class="n">checkFile</span><span class="p">(</span><span class="n">filename</span><span class="p">)</span>
	<span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
		<span class="n">logrus</span><span class="o">.</span><span class="n">Error</span><span class="p">(</span><span class="n">err</span><span class="p">)</span>
	<span class="p">}</span>

	<span class="n">file</span><span class="p">,</span> <span class="n">err</span> <span class="o">:=</span> <span class="n">ioutil</span><span class="o">.</span><span class="n">ReadFile</span><span class="p">(</span><span class="n">filename</span><span class="p">)</span>
	<span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
		<span class="n">logrus</span><span class="o">.</span><span class="n">Error</span><span class="p">(</span><span class="n">err</span><span class="p">)</span>
	<span class="p">}</span>

	<span class="n">data</span> <span class="o">:=</span> <span class="p">[]</span><span class="n">MyStruct</span><span class="p">{}</span>

	<span class="c">// Here the magic happens!</span>
	<span class="n">json</span><span class="o">.</span><span class="n">Unmarshal</span><span class="p">(</span><span class="n">file</span><span class="p">,</span> <span class="o">&amp;</span><span class="n">data</span><span class="p">)</span>

	<span class="n">newStruct</span> <span class="o">:=</span> <span class="o">&amp;</span><span class="n">MyStruct</span><span class="p">{</span>
		<span class="n">StructData</span><span class="o">:</span> <span class="s">"peanut"</span><span class="p">,</span>
	<span class="p">}</span>

	<span class="n">data</span> <span class="o">=</span> <span class="nb">append</span><span class="p">(</span><span class="n">data</span><span class="p">,</span> <span class="o">*</span><span class="n">newStruct</span><span class="p">)</span>

	<span class="c">// Preparing the data to be marshalled and written.</span>
	<span class="n">dataBytes</span><span class="p">,</span> <span class="n">err</span> <span class="o">:=</span> <span class="n">json</span><span class="o">.</span><span class="n">Marshal</span><span class="p">(</span><span class="n">data</span><span class="p">)</span>
	<span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
		<span class="n">logrus</span><span class="o">.</span><span class="n">Error</span><span class="p">(</span><span class="n">err</span><span class="p">)</span>
	<span class="p">}</span>

	<span class="n">err</span> <span class="o">=</span> <span class="n">ioutil</span><span class="o">.</span><span class="n">WriteFile</span><span class="p">(</span><span class="n">filename</span><span class="p">,</span> <span class="n">dataBytes</span><span class="p">,</span> <span class="m">0644</span><span class="p">)</span>
	<span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
		<span class="n">logrus</span><span class="o">.</span><span class="n">Error</span><span class="p">(</span><span class="n">err</span><span class="p">)</span>
	<span class="p">}</span>
<span class="p">}</span>

<span class="k">func</span> <span class="n">checkFile</span><span class="p">(</span><span class="n">filename</span> <span class="kt">string</span><span class="p">)</span> <span class="kt">error</span> <span class="p">{</span>
	<span class="n">_</span><span class="p">,</span> <span class="n">err</span> <span class="o">:=</span> <span class="n">os</span><span class="o">.</span><span class="n">Stat</span><span class="p">(</span><span class="n">filename</span><span class="p">)</span>
	<span class="k">if</span> <span class="n">os</span><span class="o">.</span><span class="n">IsNotExist</span><span class="p">(</span><span class="n">err</span><span class="p">)</span> <span class="p">{</span>
		<span class="n">_</span><span class="p">,</span> <span class="n">err</span> <span class="o">:=</span> <span class="n">os</span><span class="o">.</span><span class="n">Create</span><span class="p">(</span><span class="n">filename</span><span class="p">)</span>
		<span class="k">if</span> <span class="n">err</span> <span class="o">!=</span> <span class="no">nil</span> <span class="p">{</span>
			<span class="k">return</span> <span class="n">err</span>
		<span class="p">}</span>
	<span class="p">}</span>
	<span class="k">return</span> <span class="no">nil</span>
<span class="p">}</span>


</code></pre></div></div>
:ET